package tn.esprit.neo.rest.service.equipe;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tn.esprit.entities.Equipe;
import tn.esprit.entities.Produit;
import tn.esprit.neo.services.ServiceEquipe;

@Path("equipe")
@RequestScoped
public class equipewebservice {
	@EJB
	ServiceEquipe serviceEquipe ;


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	
	@Path("test")
	public Response cr(Equipe e) {
		
		if(serviceEquipe.AjouteEquipe(e)!=0)
			return Response.ok().build();// retourne dans postman header dans Location →http://localhost:18080/gestion-resources-humaine-web/activator/addMessage/6
		else
			return Response.notModified().build();

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Equipe> listalle() {
		return serviceEquipe.displayAll();

	}
	
}
